package net.kohding.parisguide;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.InfoWindowAdapter,
        GoogleMap.OnMapLongClickListener {

    private static final ArrayList<InfoMarker> markers = new ArrayList<>();
    private static final InfoMarker parisCenter = new InfoMarker(48.8676477, 2.3509148, 17F, "Hotel Sebastapol");
    private LinearLayout modal;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        modal = (LinearLayout) findViewById(R.id.modal);

        markers.add(parisCenter);
//        markers.add(new InfoMarker(49.4111423, -1.1835842, 9.0F, "Utah Beach"));
//        markers.add(new InfoMarker(49.3726018, -0.9026946, 13.0F, "Omaha Beach"));
//        markers.add(new InfoMarker(48.6360201, -1.5133032, 17F, "Mont Saint-Michel", "9 Euro / 1.5 Hrs from hotel"));
//        markers.add(new InfoMarker(49.3592258, -0.8554154, 17F, "American Cemetery and Memorial", "Free / Open 9-17"));
//        markers.add(new InfoMarker(49.3386353, -0.6200006, 17F, "Arromanches 360", "5 Euro / Open 1010-1710"));
//        markers.add(new InfoMarker(49.3964561, -0.9925234, 16F, "Pointe Du Hac", "Free / Open 9-18"));
//        markers.add(new InfoMarker(49.2734282, -0.7135953, 17F, "Musée Mémorial de la Bataille de Normandie", "5.50 E / 10-1230 & 14-18"));
//        markers.add(new InfoMarker(49.3501114, -0.8063523, 17F, "Normandy Hotel", "Confirmation: 13OIH6Z\nDates:13-14 Dec"));
//        markers.add(new InfoMarker(48.8597898, 2.3624461, 17F, "Musee Picasso*", "12.5E (MP) / 1030-18"));
//        markers.add(new InfoMarker(48.8564826, 2.3524726, 17F, "Hotel De Ville", "10-19"));
//        markers.add(new InfoMarker(48.8581912, 2.3615783, 17F, "Musee Cognaqc", "10-18"));
//        markers.add(new InfoMarker(48.8544675, 2.3615099, 17F, "St Paul-St Louis Church", "08-20"));
//        markers.add(new InfoMarker(48.8547705, 2.366177, 17F, "Maison de Victor Hugo", "10-18"));
//        markers.add(new InfoMarker(48.8531827, 2.3692034, 17F, "Place de la Bastille*"));
//        markers.add(new InfoMarker(48.8514093, 2.371284, 17F, "Opera Bastille*"));
//        markers.add(new InfoMarker(48.8559414, 2.3127614,  17F, "Hotel National & Invalides*"));
//        markers.add(new InfoMarker(48.8557086, 2.3126417,  17F, "Musee de l'Armee"));
//        markers.add(new InfoMarker(48.8550181, 2.3114891,  17F, "Napolean's Tomb*", "Domes Des Invalides"));
//        markers.add(new InfoMarker(48.8552048, 2.3159427,  18F, "Rodin Museum*"));
//        markers.add(new InfoMarker(48.8553843, 2.3128019,  17F, "Cathedral St Louis des Invalides*"));
//        markers.add(new InfoMarker(48.8513683, 2.301982,   18F, "Paris Military School"));
//        markers.add(new InfoMarker(48.8560357, 2.29833,    17F, "Parc du Chomp de Mars"));
//        markers.add(new InfoMarker(48.8579487, 2.294532,   17F, "Eiffel Tower*"));
//        markers.add(new InfoMarker(48.8606983, 2.2981086,  17F, "Quai Branly Museum"));
//        markers.add(new InfoMarker(48.8553423, 2.3451479,  17F, "Saint Chapelle*"));
//        markers.add(new InfoMarker(48.8557657, 2.3451574,  17F, "Palais du Justice"));
//        markers.add(new InfoMarker(48.8558984, 2.3454524,  17F, "Conciergerie*"));
//        markers.add(new InfoMarker(48.8532285, 2.3478676,  17F, "Crypte Archeologique"));
//        markers.add(new InfoMarker(48.8526858, 2.349945,   17F, "Notre Dame Cathedral*"));
//        markers.add(new InfoMarker(48.8577579, 2.3486819, 17F, "Saint Jacques Tower"));
//        markers.add(new InfoMarker(48.8604443, 2.3521484, 17F, "Pompidou*"));
//        markers.add(new InfoMarker(48.8594637, 2.3514542, 17F, "Place Igor Stravinsky"));
//        markers.add(new InfoMarker(48.8635485, 2.3451867, 17F, "St.Eustache Church"));
//        markers.add(new InfoMarker(48.8591801, 2.3411687, 17F, "Saint Germain l'Auxerrois"));
//        markers.add(new InfoMarker(48.8604276, 2.3377084, 17F, "Louvre Museum*"));
//        markers.add(new InfoMarker(48.8636402, 2.3227797, 17F, "Musee de l'Orangerie"));
//        markers.add(new InfoMarker(48.8653367, 2.3212572, 17F, "Place de la Concorde"));
//        markers.add(new InfoMarker(48.8650472, 2.3323604, 17F, "Saint Roch Church"));
//        markers.add(new InfoMarker(48.8636863, 2.3370939, 17F, "Le Palais Royal"));
//        markers.add(new InfoMarker(48.8602766, 2.3247126, 17F, "Musee National de la Legion d'Honneur"));
//        markers.add(new InfoMarker(48.8601025, 2.3265721, 17F, "Musee d'Orsay"));
//        markers.add(new InfoMarker(48.8536587, 2.3338132, 17F, "St.Germain des Pres"));
//        markers.add(new InfoMarker(48.8518024, 2.3456074, 17F, "St.Severin Church"));
//        markers.add(new InfoMarker(48.8519392, 2.3468011, 17F, "St.Julien le Pauvre"));
//        markers.add(new InfoMarker(48.8462147, 2.3463602, 17F, "Pantheon*"));
//        markers.add(new InfoMarker(48.8464474, 2.3481326, 17F, "St.Etienne du Mont"));
//        markers.add(new InfoMarker(48.8489469, 2.3574888, 17F, "Institut du Monde Arabe"));
//        markers.add(new InfoMarker(48.843976, 2.3598521, 17F, "Jardin des Plantes"));
//        markers.add(new InfoMarker(48.8406691, 2.3417248, 17F, "Val de Grace"));
//        markers.add(new InfoMarker(48.8484052, 2.3374329, 17F, "Palais du Luxembourg"));
//        markers.add(new InfoMarker(48.8510095, 2.334816, 17F, "Eglise St.Sulpice"));
//        markers.add(new InfoMarker(48.8613845, 2.289808, 17F, "Jardins du Trocadero"));
//        markers.add(new InfoMarker(48.8620845, 2.2876263, 17F, "Musee National de la Marine"));
//        markers.add(new InfoMarker(48.8626942, 2.2889728, 17F, "Cite de l'Architecture et du Patrimonie"));
//        markers.add(new InfoMarker(48.8645665, 2.2967266, 18F, "Palais de Tokyo"));
//        markers.add(new InfoMarker(48.8642927, 2.2979925, 18F, "Musee d'Art Moderne"));
//        markers.add(new InfoMarker(48.8738693, 2.2949739, 17F, "Arc de Triomphe*"));
//        markers.add(new InfoMarker(48.8659538, 2.3125402, 17F, "Grand Palais"));
//        markers.add(new InfoMarker(48.8659279, 2.3145467, 17F, "Petit Palais"));
//        markers.add(new InfoMarker(48.8706061, 2.316432, 17F, "Palais d'Elysee"));
//        markers.add(new InfoMarker(48.8639065, 2.3135912, 17F, "Pont Alexandre"));
//        markers.add(new InfoMarker(48.8048437, 2.1204198, 17F, "Palace of Versailles*"));
//        markers.add(new InfoMarker(48.8658251, 2.301777, 17F, "Crazy Horse"));
//        markers.add(new InfoMarker(48.8689837, 2.3359118, 17F, "Bols de Jean"));

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setInfoWindowAdapter(this);
        googleMap.setOnMapLongClickListener(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        } else {
            //TODO Request permissions
            Log.w("PG", "Unable to acquire location permission");
        }
//        HashMap<String, LatLng> markerMap = new HashMap<>();
        for (InfoMarker marker : markers) {
            Marker cMarker = googleMap.addMarker(marker.getMarkerOptions());
//            markerMap.put(marker.name, marker.latLng);
            if (marker == parisCenter) {
                cMarker.showInfoWindow();
            }
            if (marker.name.contains("*")) {
                cMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            }
        }

//        googleMap.addPolyline(new PolylineOptions()
//                .add(markerMap.get("Normandy Hotel"))
//                .add(markerMap.get("Musée Mémorial de la Bataille de Normandie"))
//                .add(markerMap.get("Arromanches 360"))
//                .add(markerMap.get("American Cemetery and Memorial"))
//                .add(markerMap.get("Omaha Beach"))
//                .add(markerMap.get("Pointe Du Hac"))
//                .add(markerMap.get("Utah Beach"))
//                .add(markerMap.get("Mont Saint-Michel")));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parisCenter.latLng, parisCenter.zoom));
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        Log.d("PG", marker.getTitle() + " called getInfoContents");
        View info = getLayoutInflater().inflate(R.layout.marker_info, null, false);
        return info;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        modal.setVisibility(View.VISIBLE);
        ((TextView)modal.findViewById(R.id.modal_text)).setText(latLng.toString());
    }

    //    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == MY_LOCATION_REQUEST_CODE) {
//            if (permissions.length == 1 &&
//                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
//                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                mMap.setMyLocationEnabled(true);
//            }
//        }
//    }
}
