package net.kohding.parisguide;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by kohbo on 12/12/2016.
 */

class InfoMarker {

    LatLng latLng;
    float zoom;
    String snippet;
    String name;

    InfoMarker(double lat, double lon, Float z, String n, String snip) {
        this.latLng = new LatLng(lat, lon);
        this.zoom = z;
        this.name = n;
        this.snippet = snip;
    }

    InfoMarker(double lat, double lon, Float z, String n) {
        this(lat, lon, z, n, null);
    }

    MarkerOptions getMarkerOptions() {
        return new MarkerOptions()
                .position(this.latLng)
                .title(this.name);
    }

    static void fromGMapString(String s) {
        String[] tokens = s.split(",");
    }
}
