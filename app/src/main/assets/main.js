var parisData = {
                	"Transportation": [
                		{
                			"Name": "RER B",
                			"Length": "35 Minutes",
                			"Cost": "10€",
                			"Departure": "Every 10-20 minutes",
                			"Aerogare 2": "Between terminals 2c and 2e",
                			"Aerogare 1": "5 minute walk from terminal 3",
                			"Destinations": [
                				"Gare De Nord",
                				"Chatelet Les Halles",
                				"St-Michel Notre Dame",
                				"Luxemburg",
                				"Denfert-Rochereau"
                			]
                		},
                		{
                			"Name": "Roissy Bus",
                			"Length": "60-75 Minutes",
                			"Cost": "11.50€",
                			"Departure": [
                				"Every 15-35 minutes",
                				"Hotel shuttle stop at terminal 2E",
                				"Stops at all 8 terminals except 2G"
                			],
                			"Destination": "Place Charles Garnier next to Place de l’Opera"
                		},
                		{
                			"Name": "Bus 350",
                			"Length": "60-90 minutes",
                			"Cost": "6€",
                			"Departure": "Every 15-35 Minutes",
                			"Destination": "Gare de l'est"
                		}
                	],
                	"Passes": [
                		{
                			"Name": "Navigo Pass/Decouverte",
                			"Details": [
                				"Allows unlimited travel Monday-Sunday (0000-2359)",
                				"Need 1 x 1.25in photo",
                				"Costs 22.15 € + 5 € fee"
                			]
                		},
                		{
                			"Name": "Paris Museum Pass",
                			"Cost": [
                				"2 days - 48 €",
                   				"4 days - 62 €",
                   				"6 days - 74 €"
                			],
                			"Point of Sale": [
                				"CDG terminal 1 arrivals, gate 2 ",
                				"CDG terminal 2c arrivals-departures, gate 5",
                				"CDG terminal 2d arrivals-departures, gate 7",
                				"CDG terminal 2e arrivals, gate 7",
                				"CDG terminal 2f arrivals, gate 11"
                			],
                			"Museums - Paris": [
                				"Arc de Triomphe",
                				"Musée de l’Armée - Tombeau de Napoléon 1er",
                				"Centre Pompidou - Musée national d’art moderne",
                				"Musée national des Arts asiatiques - Guimet",
                				"Musée des Arts décoratifs",
                				"Espaces Mode et Textile",
                				"Espaces Publicité",
                				"Musée Nissim de Camondo",
                				"Musée des Arts et Métiers",
                				"Musée du quai Branly - Jacques Chirac",
                				"Chapelle expiatoire",
                				"La Cinémathèque française - Musée du Cinéma",
                				"Cité des Sciences et de l’Industrie - universcience",
                				"Conciergerie",
                				"Musée national Eugène Delacroix",
                				"Visite publique des Égouts de Paris",
                				"Palais de la Porte Dorée - Musée national de l'histoire de l'Immigration",
                				"Musée de l’Institut du Monde arabe",
                				"Musée d'art et d'histoire du Judaïsme",
                				"Musée du Louvre",
                				"Musée national de la Marine",
                				"Cité de l’Architecture et du Patrimoine - Musée des Monuments français",
                				"Musée Gustave Moreau",
                				"Musée de Cluny - Musée national du Moyen Âge",
                				"Philharmonie de Paris - Musée de la musique",
                				"Crypte archéologique du Parvis Notre-Dame",
                				"Tours de Notre-Dame",
                				"Musée national de l’Orangerie",
                				"Musée de l’Ordre de la Libération",
                				"Musée d’Orsay",
                				"Palais de la découverte - universcience",
                				"Panthéon",
                				"Musée national Picasso-Paris",
                				"Musée des Plans-reliefs",
                				"Musée Rodin",
                				"Sainte-Chapelle"
                			],
                			"Museums - Paris Region": [
                				"Musée de l’Air et de l’Espace",
                				"Musée d’Archéologie nationale et  Domaine national de Saint-Germain-en-Laye",
                				"Sèvres, Cité de la céramique - Musée national de la céramique",
                				"Abbaye royale de Chaalis",
                				"Château de Champs-sur-Marne",
                				"Musées et domaine nationaux du Palais de Compiègne",
                				"Musée Condé - Château de Chantilly",
                				"Musée départemental Maurice Denis",
                				"Château de Fontainebleau",
                				"Château de Maisons",
                				"Musée national du château de Malmaison",
                				"Château de Pierrefonds",
                				"Musée national de Port-Royal des Champs",
                				"Château de Rambouillet, Laiterie de la Reine et Chaumière aux Coquillages",
                				"Musée national de la Renaissance - Château d’Ecouen",
                				"Maison d’Auguste Rodin à Meudon",
                				"Basilique cathédrale de Saint-Denis",
                				"Villa Savoye",
                				"Châteaux de Versailles et de Trianon",
                				"Château de Vincennes"
                			]
                		}
                	],
                	"Attractions": {
                		"Ile de la Cite": [
                			[
                				"Saint Chapelle",
                				true,
                				"https:\/\/lh6.googleusercontent.com/z8wHVLNbH_lcFfILuO0z9to2bxsQII2VoaYxKvFio46gwtmz65YUzfITbBvpjmIUR7uVQkBsyGxbQlswc-ijQcmFo4F0BIH2FH3M4x_QoB9ehyyo0P0lf4z_MuhLRhgUBj8DKL1Y",
                				"Church built in 1248 by Louis XI to house the Crown of Thorns and the True Cross",
                				"15 stained glass windows with 1,113 scenes from the old and new testament",
                				"Every Day 0900-1700",
                				"10€ (13.50€ with Concierge)",
                				[
                					"Metro: Cite line 4 or St.Michel",
                					"RER: St. Michel",
                					"Bus: 21, 27, 38, 85, 96"
                				]
                			]
                		],
                		"Marais": []
                	}
                };

var paris = {
    activeSection: null,
    showSection: function(sectionName){
        console.log("Attempting to show " + sectionName);
        var sectionDiv = document.getElementById(sectionName);
        if(!sectionDiv){
            sectionDiv = this['build'+sectionName](this.buildSectionDiv(sectionName));
            document.getElementById("content").appendChild(sectionDiv);
        }

        if(this.activeSection != null && this.activeSection != sectionDiv){
            this.activeSection.style.display = "none";
            this.activeSection = sectionDiv;
            this.activeSection.style.display = "initial";
        }
        this.activeSection = sectionDiv;
        this.activeSection.style.display = "initial";
    },
    buildBtn: function(name){
        var menuBtn = document.createElement("button");
        menuBtn.className = "mdl-button mdl-js-button mdl-button--raised";
        menuBtn.innerText = name;
        menuBtn.onclick = function(){
            console.log("Clicked on " + name);
            paris.showSection(name);
        };
        return menuBtn;
    },
    buildMenu: function(menuDiv){
        for(var sectionName in parisData){
            menuDiv.appendChild(this.buildBtn(sectionName));
        }
        return menuDiv;
    },
    buildSectionDiv: function(name){
        console.log("Building section div for " + name);
        var sectionDiv = document.createElement("div");
        sectionDiv.style.display = "none";
        sectionDiv.id = name;
        sectionDiv.innerHTML = "<h2>"+name+"</h2>"
        return sectionDiv;
    },
    buildCard: function(title, details, background){
        var cardDiv = document.createElement("div");
        cardDiv.className = "card-wide mdl-card mdl-shadow--2dp";
        cardDiv.innerHTML = '<div class="mdl-card__title"><h2 class="mdl-card__title-text">' + title + '</h2></div>'+
                            '<div class="mdl-card__supporting-text">'+details+'</div>';
//        var cardTitleDiv = document.createElement("div");
//        cardTitleDiv.className = "mdl-card__title";
//        var cardTitleTextDiv = document.createElement("div");
//        cardTitleTextDiv.className = "mdl-card__title-text";
//        cardTitleTextDiv.innerText = title;
//        cardTitleDiv.appendChild(cardTitleTextDiv);
//        cardDiv.appendChild(cardTitleDiv);

        return cardDiv;

    },
    buildDetailsHtml: function(transportationItem){
    	var resultHtml = "";
    	for(var propName in transportationItem){
    		if(transportationItem.hasOwnProperty(propName)){
    			var propValue = transportationItem[propName];
    			resultHtml += '<li class="mdl-list__item"><span class="mdl-list__item-primary-content">' + propName + '</span>';
    			if(typeof(propValue) == "string"){
    				resultHtml += '<span class="mdl-list__item-secondary-content">' + propValue + '</span>';
    			}
    			if(propValue.__proto__.constructor.name == "Array"){
    				resultHtml += '<div class="mdl-list__item-secondary-content"><ul class="mdl-list">';
    				for(var arrayItem of propValue){
    					resultHtml += '<li class="mdl-list__item">' + arrayItem + '</li>';
    				}
    				resultHtml += '</ul></div>';
    			}
    			resultHtml += '</li>';
    		}
    	}
    	return '<ul class="mdl-list">' + resultHtml + '</ul>'
    },
    buildTransportation: function(sectionDiv){
        var transportationItems = parisData[sectionDiv.id];

        for(var item of transportationItems){
        	var details = {
        		Cost: item.Cost,
        		Length: item.Length,
        		Departure: item.Departure
        	};
            var itemCard = this.buildCard(item["Name"], this.buildDetailsHtml(details));
            sectionDiv.appendChild(itemCard);
        }
        return sectionDiv;
    },
    showTransportationDetails: function(transportationItem){
    	this.showSection("DetailsHtml")
    },
    buildPasses: function(sectionDiv){
    	
    }

};